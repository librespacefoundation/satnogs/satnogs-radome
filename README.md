# SatNOGS Radome

3V 6/8 Radome design.

More information can be found in our [documentation](https://satnogs.org/documentation/hardware/).

## License

&copy; 2014-2015 [Libre Space Foundation](http://librespacefoundation.org).

Licensed under the [CERN OHLv1.2](LICENSE).
